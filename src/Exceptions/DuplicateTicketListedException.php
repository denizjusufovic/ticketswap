<?php

namespace TicketSwap\Assessment\Exceptions;

use TicketSwap\Assessment\Ticket;

final class DuplicateTicketListedException extends \Exception
{
    public static function withTicket(Ticket $ticket) : self
    {
        return new self(
            sprintf(
                'Ticket %s is already listed for sale in another listing!',
                (string) $ticket->getId()
            )
        );
    }
}