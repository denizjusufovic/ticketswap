<?php

namespace TicketSwap\Assessment\Exceptions;

use TicketSwap\Assessment\Barcode;

final class DuplicateBarcodesException extends \Exception
{
    public static function withBarcode(Barcode $barcode) : self
    {
        return new self(
            sprintf(
                'Ticket with barcode %s is already listed for sale!',
                (string) $barcode
            )
        );
    }
}