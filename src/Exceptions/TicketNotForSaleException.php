<?php

namespace TicketSwap\Assessment\Exceptions;

use TicketSwap\Assessment\TicketId;

final class TicketNotForSaleException extends \Exception
{
    public static function withTicketId(TicketId $ticketId) : self
    {
        return new self(
            sprintf(
                'Ticket %s is not for sale.',
                (string) $ticketId
            )
        );
    }
}