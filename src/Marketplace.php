<?php

namespace TicketSwap\Assessment;

use TicketSwap\Assessment\Exceptions\DuplicateTicketListedException;
use TicketSwap\Assessment\Exceptions\TicketNotForSaleException;

final class Marketplace
{
    /**
     * @param array<Listing> $listingsForSale
     */
    public function __construct(private array $listingsForSale = [])
    {
    }

    /**
     * @return array<Listing>
     */
    public function getListingsForSale() : array
    {
        return array_filter($this->listingsForSale, function ($listing) {
            return $listing->getIsForSale() && $listing->getIsVerified();
        });
    }

    /**
     * @throws Exceptions\TicketAlreadySoldException
     * @throws TicketNotForSaleException
     */
    public function buyTicket(Buyer $buyer, TicketId $ticketId) : Ticket
    {
        foreach($this->getListingsForSale() as $listing) {
            foreach($listing->getTickets() as $ticket) {
                if ($ticket->getId()->equals($ticketId)) {
                    /**
                     * Once all tickets have been sold for a listing, it is no longer for sale.
                     *
                     * Ideally this code wouldn't go here.I would rather just trigger an Event that a ticket has been bought
                     * and in a Listener I would handle the logic for setting the listing to not-for-sale, so we can only have Ticket logic here
                     */
                    $ticket->buyTicket($buyer);
                    if(count($listing->getTickets(forSale: true)) == 0) {
                        $listing->setNotForSale();
                    }

                    return $ticket;
                }
            }
        }
        throw TicketNotForSaleException::withTicketId($ticketId);
    }

    /**
     * @throws DuplicateTicketListedException
     */
    public function setListingForSale(Listing $listing) : void
    {
        $this->validateForDuplicateTickets($listing);

        $this->listingsForSale[] = $listing;
    }

    /**
     * Ideally of course we would do this by query, instead of just loading all objects in-memory and filtering them
     * Also nested for loops should be avoided, and this one with cardinality of n^3 would have awful performance on
     * real scale data
     *
     * @throws DuplicateTicketListedException
     */
    private function validateForDuplicateTickets(Listing $newListing)
    {
        $barcodes = [];
        foreach($this->getListingsForSale() as $listing) {
            foreach($listing->getTickets() as $ticket) {
                foreach($ticket->getBarcodes() as $barcode) {
                    $barcodes[(string) $barcode] = (string) $ticket->getBuyer();
                }
            }
        }

        foreach($newListing->getTickets() as $ticket) {
            foreach ($ticket->getBarcodes() as $barcode) {
                if (array_key_exists((string) $barcode, $barcodes) && $barcodes[(string) $barcode] != (string) $newListing->getSeller()) {
                    throw DuplicateTicketListedException::withTicket($ticket);
                }
            }
        }
    }
}
