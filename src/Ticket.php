<?php

namespace TicketSwap\Assessment;

use TicketSwap\Assessment\Exceptions\TicketAlreadySoldException;

final class Ticket
{
    /**
     * @param array<Barcode> $barcodes
     */
    public function __construct(private TicketId $id, private array $barcodes, private ?Buyer $buyer = null)
    {
    }

    public function getId() : TicketId
    {
        return $this->id;
    }

    /**
     * Tickets can contain multiple barcodes
     * @return Barcode[]
     */
    public function getBarcodes() : array
    {
        return $this->barcodes;
    }

    public function getBuyer() : Buyer|null
    {
        return $this->buyer;
    }

    public function isBought() : bool
    {
        return $this->buyer !== null;
    }

    /**
     * @throws TicketAlreadySoldException
     */
    public function buyTicket(Buyer $buyer) : self
    {
        if ($this->isBought()) {
            throw TicketAlreadySoldException::withTicket($this);
        }

        $this->buyer = $buyer;

        return $this;
    }
}
