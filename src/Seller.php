<?php

namespace TicketSwap\Assessment;

use Money\Currency;
use Money\Money;
use Ramsey\Uuid\Uuid;

final class Seller implements \Stringable
{
    public function __construct(private string $name)
    {
    }

    public function __toString() : string
    {
        return $this->name;
    }

    /**
     * @param Ticket[] $tickets
     * @throws Exceptions\DuplicateBarcodesException
     */
    public function createListing(array $tickets) : Listing
    {
        return new Listing(
            id: new ListingId(Uuid::uuid4()),
            seller: new Seller($this->name),
            tickets: $tickets,
            price: new Money(rand(3000, 6000), new Currency('EUR')),
        );
    }
}
