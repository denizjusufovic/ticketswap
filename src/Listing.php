<?php

namespace TicketSwap\Assessment;

use Money\Money;
use TicketSwap\Assessment\Exceptions\DuplicateBarcodesException;

final class Listing
{
    /**
     * @param array<Ticket> $tickets
     * @throws DuplicateBarcodesException
     */
    public function __construct(
        private ListingId $id,
        private Seller $seller,
        private array $tickets,
        private Money $price,
        private bool $isForSale = true,
        private bool $isVerified = false
    ) {
        // It should not be possible to create a listing with duplicate barcodes in it
        $this->checkForDuplicateTickets($this->tickets);
    }

    public function getId() : ListingId
    {
        return $this->id;
    }

    public function getSeller() : Seller
    {
        return $this->seller;
    }

    public function getIsForSale() : bool
    {
        return $this->isForSale;
    }

    /**
     * For the purpose of having a listings' history, I decided not to delete a listing when all tickets are bought,
     * but rather set its for-sale state to false
     */
    public function setNotForSale() : void
    {
        $this->isForSale = false;
    }

    public function getIsVerified() : bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified = true) : void
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return array<Ticket>
     */
    public function getTickets(?bool $forSale = null) : array
    {
        if (true === $forSale) {
            $forSaleTickets = [];
            foreach ($this->tickets as $ticket) {
                if (!$ticket->isBought()) {
                    $forSaleTickets[] = $ticket;
                }
            }

            return $forSaleTickets;
        } else if (false === $forSale) {
            $notForSaleTickets = [];
            foreach ($this->tickets as $ticket) {
                if ($ticket->isBought()) {
                    $notForSaleTickets[] = $ticket;
                }
            }

            return $notForSaleTickets;
        } else {
            return $this->tickets;
        }
    }

    public function getPrice() : Money
    {
        return $this->price;
    }

    /**
     * @throws DuplicateBarcodesException
     */
    private function checkForDuplicateTickets(array $tickets)
    {
        $allBarcodes = [];
        foreach($tickets as $ticket) {
            $ticketBarcodes = $ticket->getBarcodes();
            foreach($ticketBarcodes as $ticketBarcode) {
                if(!in_array($ticketBarcode, $allBarcodes)) {
                    $allBarcodes[] = $ticketBarcode;
                    continue;
                }
                throw DuplicateBarcodesException::withBarcode($ticketBarcode);
            }
        }
    }
}
