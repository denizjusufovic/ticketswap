<?php

namespace TicketSwap\Assessment\tests;

use PHPUnit\Framework\TestCase;
use TicketSwap\Assessment\Barcode;
use TicketSwap\Assessment\Exceptions\DuplicateBarcodesException;
use TicketSwap\Assessment\Seller;
use TicketSwap\Assessment\Ticket;
use TicketSwap\Assessment\TicketId;

class SellerTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_be_possible_for_the_seller_to_create_a_listing(){
        $seller = new Seller("Deniz");
        $listing = $seller->createListing(
            [
                new Ticket(
                    new TicketId('1111BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                    [
                        new Barcode('EAN-13', '18974312923')
                    ]
                ),
                new Ticket(
                    new TicketId('2222BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                    [
                        new Barcode('EAN-13', '28974312923')
                    ]
                ),
            ]
        );

        $this->assertCount(2, $listing->getTickets());
        $this->assertSame("Deniz", (string) $listing->getSeller());
    }

    /**
     * @test
     */
    public function it_should_not_be_possible_for_the_seller_to_create_a_listing_with_duplicate_tickets()
    {
        $seller = new Seller("Deniz");

        $this->expectException(DuplicateBarcodesException::class);

        $seller->createListing(
            [
                new Ticket(
                    new TicketId('1111BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                    [
                        new Barcode('EAN-13', '18974312923')
                    ]
                ),
                new Ticket(
                    new TicketId('1111BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                    [
                        new Barcode('EAN-13', '18974312923')
                    ]
                ),
            ]
        );
    }
}