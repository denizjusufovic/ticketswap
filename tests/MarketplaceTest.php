<?php

namespace TicketSwap\Assessment\tests;

use PHPUnit\Framework\TestCase;
use Money\Currency;
use Money\Money;
use TicketSwap\Assessment\Barcode;
use TicketSwap\Assessment\Buyer;
use TicketSwap\Assessment\Exceptions\DuplicateBarcodesException;
use TicketSwap\Assessment\Exceptions\DuplicateTicketListedException;
use TicketSwap\Assessment\Exceptions\TicketNotForSaleException;
use TicketSwap\Assessment\Listing;
use TicketSwap\Assessment\ListingId;
use TicketSwap\Assessment\Marketplace;
use TicketSwap\Assessment\Seller;
use TicketSwap\Assessment\Ticket;
use TicketSwap\Assessment\Exceptions\TicketAlreadySoldException;
use TicketSwap\Assessment\TicketId;

class MarketplaceTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_list_all_the_tickets_for_sale_from_a_verified_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $listingsForSale = $marketplace->getListingsForSale();

        $this->assertCount(1, $listingsForSale);
    }
    /**
     * @test
     */
    public function it_should_not_list_tickets_for_sale_from_a_non_verified_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR'))
                ),
            ]
        );

        $listingsForSale = $marketplace->getListingsForSale();

        $this->assertCount(0, $listingsForSale);
    }

    /**
     * @test
     */
    public function it_should_be_possible_to_buy_a_ticket_from_a_verified_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $boughtTicket = $marketplace->buyTicket(
            buyer: new Buyer('Sarah'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );

        $this->assertNotNull($boughtTicket);

        $barcodes = [];
        foreach($boughtTicket->getBarcodes() as $barcode) {
            $barcodes[] = (string) $barcode;
        }

        $this->assertContains('EAN-13:38974312923', $barcodes);
    }

    /**
     * @test
     */
    public function it_should_not_be_possible_to_buy_a_ticket_from_a_non_verified_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                ),
            ]
        );

        $this->expectException(TicketNotForSaleException::class);
        $this->expectExceptionMessage('Ticket 6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B is not for sale.');

        $marketplace->buyTicket(
            buyer: new Buyer('Sarah'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );
    }

    /**
     * @test
     */
    public function it_should_not_be_possible_to_buy_the_same_ticket_twice_from_a_verified_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                        new Ticket(
                            new TicketId('TESTBB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '11114312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $marketplace->buyTicket(
            buyer: new Buyer('Sarah'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );

        $this->expectException(TicketAlreadySoldException::class);
        $this->expectExceptionMessage("Ticket (6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B) has already been sold");

        $marketplace->buyTicket(
            buyer: new Buyer('Deniz'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );
    }

    /**
     * Automatically when the last ticket is bought from a listing, the listing's is-for-sale status is set to false
     *
     * @test
     */
    public function it_should_not_be_possible_to_buy_ticket_from_closed_verified_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $marketplace->buyTicket(
            buyer: new Buyer('Sarah'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );

        $this->expectException(TicketNotForSaleException::class);
        $this->expectExceptionMessage("Ticket 6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B is not for sale.");

        $marketplace->buyTicket(
            buyer: new Buyer('Deniz'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );
    }

    /**
     * @test
     */
    public function it_should_be_possible_to_put_a_listing_for_sale()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $marketplace->setListingForSale(
            new Listing(
                id: new ListingId('26A7E5C4-3F59-4B3C-B5EB-6F2718BC31AD'),
                seller: new Seller('Tom'),
                tickets: [
                    new Ticket(
                        new TicketId('45B96761-E533-4925-859F-3CA62182848E'),
                        [
                            new Barcode('EAN-13', '00074312923')
                        ]
                    ),
                ],
                price: new Money(4950, new Currency('EUR')),
                isVerified: true
            )
        );

        $listingsForSale = $marketplace->getListingsForSale();

        $this->assertCount(2, $listingsForSale);
    }

    /**
     * @test
     */
    public function it_should_be_possible_to_put_a_listing_for_sale_containing_ticket_from_another_listing_that_has_been_bought_by_new_listing_seller()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923') // same barcode
                            ]
                        ),
                        new Ticket(
                            new TicketId('TESTBB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '11174312923') // same barcode
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                )
            ]
        );

        $boughtTicket = $marketplace->buyTicket(
            buyer: new Buyer('Deniz'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );

        $marketplace->setListingForSale(
            new Listing(
                id: new ListingId('DENIZ5C4-3F59-4B3C-B5EB-6F2718BC31AD'),
                seller: new Seller((string) $boughtTicket->getBuyer()),
                tickets: [
                    $boughtTicket
                ],
                price: new Money(4950, new Currency('EUR')),
                isVerified: true
            )
        );

        $this->assertCount(2, $marketplace->getListingsForSale());
        $this->assertSame('D59FDCCC-7713-45EE-A050-8A553A0F1169', (string) $marketplace->getListingsForSale()[0]->getId());
        $this->assertSame('DENIZ5C4-3F59-4B3C-B5EB-6F2718BC31AD', (string) $marketplace->getListingsForSale()[1]->getId());
    }

    /**
     * @test
     */
    public function listing_put_for_sale_is_not_verified_by_default()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                ),
            ]
        );

        $marketplace->setListingForSale(
            new Listing(
                id: new ListingId('26A7E5C4-3F59-4B3C-B5EB-6F2718BC31AD'),
                seller: new Seller('Tom'),
                tickets: [
                    new Ticket(
                        new TicketId('45B96761-E533-4925-859F-3CA62182848E'),
                        [
                            new Barcode('EAN-13', '38974312923')
                        ]
                    ),
                ],
                price: new Money(4950, new Currency('EUR')),
            )
        );

        $listingsForSale = $marketplace->getListingsForSale();

        $this->assertCount(0, $listingsForSale);
    }

    /**
     * @test
     */
    public function it_should_not_be_possible_to_sell_a_ticket_with_a_barcode_that_is_already_for_sale_in_the_same_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923')
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $this->expectException(DuplicateBarcodesException::class);

        $marketplace->setListingForSale(
            new Listing(
                id: new ListingId('26A7E5C4-3F59-4B3C-B5EB-6F2718BC31AD'),
                seller: new Seller('Tom'),
                tickets: [
                    new Ticket(
                        new TicketId('45B96761-E533-4925-859F-3CA62182848E'),
                        [
                            new Barcode('EAN-13', '38974312923'),
                            new Barcode('EAN-14', '12345678910')
                        ]
                    ),
                    new Ticket(
                        new TicketId('45B96761-E533-4925-859F-3CA62182848E'),
                        [
                            new Barcode('EAN-13', '38974312923'), // same barcode
                            new Barcode('EAN-14', '10987654321')
                        ]
                    ),
                ],
                price: new Money(4950, new Currency('EUR')),
            )
        );
    }

    /**
     * @test
     */
    public function it_should_not_be_possible_to_sell_a_ticket_with_a_barcode_that_is_already_for_sale_in_any_listing()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923') // same barcode
                            ]
                        ),
                    ],
                    price: new Money(4950, new Currency('EUR')),
                    isVerified: true
                )
            ]
        );

        $this->expectException(DuplicateTicketListedException::class);
        $this->expectExceptionMessage("Ticket 45B96761-E533-4925-859F-3CA62182848E is already listed for sale in another listing!");

        $marketplace->setListingForSale(
            new Listing(
                id: new ListingId('26A7E5C4-3F59-4B3C-B5EB-6F2718BC31AD'),
                seller: new Seller('Tom'),
                tickets: [
                    new Ticket(
                        new TicketId('45B96761-E533-4925-859F-3CA62182848E'),
                        [
                            new Barcode('EAN-13', '38974312923'), // same barcode
                            new Barcode('EAN-14', '10987654321')
                        ]
                    ),
                ],
                price: new Money(4950, new Currency('EUR')),
                isVerified: true
            )
        );
    }

    /**
     * @test
     */
    public function it_should_be_possible_for_a_buyer_of_a_ticket_to_sell_it_again()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923'),
                                new Barcode('EAN-14', '58974312911')
                            ]
                        ),
                        new Ticket(
                            new TicketId('TESTBB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '12345678910'),
                            ]
                        ),
                    ],
                    price: new Money(23, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $boughtTicket = $marketplace->buyTicket(
            buyer: new Buyer('Deniz'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );

        $marketplace->setListingForSale(
            new Listing(
                id: new ListingId('TESTE5C4-3F59-4B3C-B5EB-6F2718BC31AD'),
                seller: new Seller((string) $boughtTicket->getBuyer()),
                tickets: [
                    new Ticket(
                        new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                        [
                            new Barcode('EAN-13', '38974312923'),
                            new Barcode('EAN-14', '58974312911')
                        ]
                    ),
                ],
                price: new Money(4950, new Currency('EUR')),
                isVerified: true
            )
        );

        $listingsForSale = $marketplace->getListingsForSale();

        $this->assertCount(2, $listingsForSale);
    }

    /**
     * @test
     */
    public function listing_should_not_be_available_in_the_marketplace_once_all_tickets_have_been_sold()
    {
        $marketplace = new Marketplace(
            listingsForSale: [
                new Listing(
                    id: new ListingId('D59FDCCC-7713-45EE-A050-8A553A0F1169'),
                    seller: new Seller('Pascal'),
                    tickets: [
                        new Ticket(
                            new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '38974312923'),
                                new Barcode('EAN-14', '58974312911')
                            ]
                        ),
                        new Ticket(
                            new TicketId('TESTBB44-2F5F-4E2A-ACA8-8CDF01AF401B'),
                            [
                                new Barcode('EAN-13', '18974312923'),
                                new Barcode('EAN-14', '28974312911')
                            ]
                        ),
                    ],
                    price: new Money(200, new Currency('EUR')),
                    isVerified: true
                ),
            ]
        );

        $marketplace->buyTicket(
            buyer: new Buyer('Deniz'),
            ticketId: new TicketId('TESTBB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );

        $marketplace->buyTicket(
            buyer: new Buyer('Not Deniz'),
            ticketId: new TicketId('6293BB44-2F5F-4E2A-ACA8-8CDF01AF401B')
        );

        $this->assertCount(0, $marketplace->getListingsForSale());
    }
}
