# TicketSwap assessment - Solution
1. All the business logic from the assignment is implemented.
2. All the unit tests pass. Some have been changed to fit the new logic, and there are many new ones added.
3. The **Bonus** request (with the verified listings) is implemented as well, along with unit tests. Of course the existing unit tests have been modified accordingly.
4. There are some PHPDocs and comments where explanation of the implemented logic is written. Just so you can see my mindset at the time of coding.

### The assignment

There are some concrete classes for implementation and there are some unit tests. Your job is to:
- make sure all the business logic above is implemented and tested
- make sure all the tests pass
- implement the missing functionality in the skipped tests

The business rules are as follows:
- A listing contains multiple tickets. 
- Tickets can contain **multiple** barcodes.
- Sellers can create listings with tickets.
- Buyers can buy individual tickets from a listing.
- Once all tickets have been sold for a listing, it is no longer for sale.
- It should not be possible to create a listing with duplicate barcodes in it.
- It should not be possible to create a listing with duplicate barcodes within another listing.
- Though, it should be possible for the last buyer of a ticket, to create a listing with that ticket (based on barcode).